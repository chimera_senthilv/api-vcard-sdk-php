<?php

class MatchMovePay {
    const NS = 'MatchMovePay\\';
    const EXT = '.php';
    
    public static function init() {
        spl_autoload_register('MatchMovePay::autoload');
    }
    
    public static function autoload ($class) {
        if (0 !== strpos($class, MatchMovePay::NS)) {
            return true;
        }
        
        $class = explode
            ('\\',
                strtolower(
                    substr($class, strlen(MatchMovePay::NS))));

        $file = __DIR__ . DIRECTORY_SEPARATOR
            . implode(DIRECTORY_SEPARATOR, $class) . MatchMovePay::EXT;
        
        if (!is_file($file)) {
            throw new Exception('Cannot load `' . $file . '`.');
        }

        require_once $file;
    }
}