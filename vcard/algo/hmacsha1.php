<?php
namespace MatchMovePay\VCard\Algo;

class HMACSHA1 extends \MatchMovePay\VCard\Algo {

    public function __construct($payload, $key) {
        parent::__construct(hash_hmac('sha1', $payload, $key, true));
    }
}

